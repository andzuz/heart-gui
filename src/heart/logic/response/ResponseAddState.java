package heart.logic.response;

import java.util.List;

public class ResponseAddState extends Response {
    private List<String> content;

    public ResponseAddState(String raw,
                               List<String> content) {
        super(raw);
        this.content = content;
    }

    public String[] asArray() {
        String[] res = new String[content.size()];
        return content.toArray(res);
    }
}
