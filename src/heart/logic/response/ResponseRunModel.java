package heart.logic.response;

import java.util.*;

public class ResponseRunModel extends Response {
    private List<String> systemState;
    private String systemTrajectory;

    public ResponseRunModel(String raw,
                  List<String> state, String trajectory) {
        super(raw);
        systemState = state;
        systemTrajectory = trajectory;
    }

    public String[] getState() {
        String[] res = new String[systemState.size()];
        return systemState.toArray(res);
    }

    public String getTrajectory() {
        return systemTrajectory;
    }
}
