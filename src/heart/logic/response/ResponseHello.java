package heart.logic.response;

public class ResponseHello extends Response {
    private String protocolInfo;

    public ResponseHello(String raw, String protInf) {
        super(raw);
        protocolInfo = protInf;
    }

    //zmiana nazwy co do diagramu
    public String getInfo() { return  protocolInfo; }
}
