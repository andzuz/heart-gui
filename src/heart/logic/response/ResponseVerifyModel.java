package heart.logic.response;

import java.util.*;

public class ResponseVerifyModel extends Response {
    private List<String> content;

    public ResponseVerifyModel(String raw,
                            List<String> content) {
        super(raw);
        this.content = content;
    }

    public String[] asArray() {
        String[] res = new String[content.size()];
        return content.toArray(res);
    }
}
