package heart.logic.response;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

public class ResponseGetlistModel extends Response {
    private List<String> modelnameUsername;

    public ResponseGetlistModel(String raw,
                               List<String> modelUser) {
        super(raw);
        modelnameUsername = modelUser;
    }

    public String[] asArray() {
        String[] res = new String[modelnameUsername.size()];
        return modelnameUsername.toArray(res);
    }

	public LinkedHashMap<String, Vector<String>> asHashMap() {
		LinkedHashMap<String, Vector<String>> temp = new LinkedHashMap<String, Vector<String>>();

		for(int i=0; i<modelnameUsername.size(); i++) {
			String[] t = modelnameUsername.get(i).split(",");

			if(temp.containsKey(t[1]))
				temp.get(t[1]).add(t[0]);
			else {
				temp.put(t[1], new Vector<String>());
				temp.get(t[1]).add(t[0]);
			}
		}

		return temp;
	}
}
