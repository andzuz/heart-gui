package heart.logic.response;

public class ResponseExistsModel extends Response {
    private boolean doesExist;

    public ResponseExistsModel(String raw, boolean exists) {
        super(raw);
        doesExist = exists;
    }

    public boolean exists() { return doesExist; }
}
