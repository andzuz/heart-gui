package heart.logic;

import heart.logic.parsing.Parser;
import heart.logic.request.*;
import heart.logic.response.*;
import heart.net.ConnectionHandler;

import java.awt.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by: Piotrek
 * Date: 01.02.13
 * Time: 14:13
 */

public class TestClass {
	/*public static void main(String[] args) throws Exception {
		TestClass test = new TestClass();

		test.connectionTest();
	}             */

    public static void main(String[] args) throws Exception {
        ConnectionHandler ch = null;
        try {
            ch = new ConnectionHandler("localhost", 8090, new Parser());

            ResponseGetlistModel rgetlist = (ResponseGetlistModel) ch.sendAndGet(new RequestGetlistModel(), RespType.MODEL_GETLIST);
            System.out.println(rgetlist == null);

            ResponseHello respHello = (ResponseHello) ch.sendAndGet(new RequestHello("Piotrek"), RespType.HELLO);
            System.out.println(respHello.getInfo());

            RequestExistModel exModel = new RequestExistModel("user2","thermostat");
            ResponseExistsModel respExModel = (ResponseExistsModel) ch.sendAndGet(exModel, RespType.MODEL_EXISTS);
            System.out.println(respExModel.getRaw());

            //możnaby testować jeszcze inne, ale widać już że działa :D
        } finally {
            if(ch != null)
                ch.disconnect();
        }
    }

	private void openHeartInProlog() throws IOException, AWTException, InterruptedException {
		String pathToFile = "C:\\Users\\Piotrek\\Desktop\\heart\\heart.pl";
		Process p = Runtime.getRuntime().exec(new String[] {"cmd.exe", "/C", pathToFile});
	}

	//do testu potrzeba uruchomic heart przez console PROLOG'a, nastepnie consult heart.pl,
	//potem wcisnac spacje i wpisac srv. no i powinno być
	public void connectionTest() throws IOException {
		ConnectionHandler ch = null;
		try {
			ch = new ConnectionHandler("localhost", 8090, new Parser());

			RequestExistModel exModel = new RequestExistModel("user2","thermostat");
			ResponseExistsModel respExModel = (ResponseExistsModel) ch.sendAndGet(exModel, RespType.MODEL_EXISTS);
			System.out.println(respExModel.getRaw());

			RequestGetlistModel getList = new RequestGetlistModel();
			ResponseGetlistModel respGetList = (ResponseGetlistModel) ch.sendAndGet(getList, RespType.MODEL_GETLIST);
			System.out.println(respGetList.getRaw());

			ResponseHello respHello = (ResponseHello) ch.sendAndGet(new RequestHello("Piotrek"), RespType.HELLO);
			System.out.println(respHello.getInfo());

			//możnaby testować jeszcze inne, ale widać już że działa :D
		} finally {
			if(ch != null)
				ch.disconnect();
		}
	}

	// statyczny test sprawdzający poprawność parsowania tekstów
	public void staticParserTest() {
		Parser par = new Parser();

		System.out.println("RESPONSE TEST: \n");

		//TESTOWANIE RESPONSE ON -------------
		//HELLO
		ResponseHello rhello = (ResponseHello) par.stringToResponse(
				"[true,[heart,hello,1.0,5,[]]]", RespType.HELLO);
		System.out.println("HELLO** " + rhello.getInfo());

		//GETLIST
		ResponseGetlistModel rgetlist =
				(ResponseGetlistModel) par.stringToResponse(
						"[true,[[modelname1,username1],[modelname2,username1],[modelname3,username2]]]"
						,RespType.MODEL_GETLIST);
		System.out.println("GETLIST** " + Arrays.toString(rgetlist.asArray()));

		//VERIFY MODEL
		ResponseVerifyModel rverify =
				(ResponseVerifyModel) par.stringToResponse(
						"[true,[[4, 'In os rule: 4 can be joined with rule 8'],[8, 'In os rule: 8 can be joined with rule 4']]]"
						,RespType.MODEL_VERIFY);
		System.out.println("VERIFY** " + Arrays.toString(rverify.asArray()));

		//ERROR
		ResponseError err = (ResponseError) par.stringToResponse("[false,'Command not supported.']", RespType.HELLO);
		System.out.println("ERROR** " + err.getRaw());

		//MODEL GET
		ResponseGetModel rget = (ResponseGetModel) par.stringToResponse(
				"[true,[xtype[name:week_days, base:symbolic, ordered:yes, " +
						"domain:[moday, tuesday], desc:'This is only one definition'].]]",
				RespType.MODEL_GET);
		System.out.println("GETMODEL** " + rget.getModelContent());

		//EXISTS
		ResponseExistsModel rexists = (ResponseExistsModel) par.stringToResponse(
				"[true,false]", RespType.MODEL_EXISTS);
		System.out.println("EXISTS** " + rexists.exists());

		//MODEL RUN
		ResponseRunModel rrun = (ResponseRunModel) par.stringToResponse(
				"[true,[[day, 3.0],[hour, 3.0],[month, 2.0],[season, 3.0],[today, 1.0],[operation, 2.0],[thermostat_settings, 16.0]],[ms, 4, dt, 1, th, 3, os, 7]]",
				RespType.MODEL_RUN);
		System.out.println("RUN** " + Arrays.toString(rrun.getState()) + '\n' + rrun.getTrajectory());
		//TESTOWANIE RESPONSE OFF -------------

		System.out.println("\nREQUEST TEST: \n");

		//TESTOWANIE REQUEST ON -------------
		//HELLO
		RequestHello rqhello = new RequestHello("Piotrek");
		System.out.println(par.RequestToString(rqhello));

		//GETLIST MODEL
		RequestGetlistModel rqgetlistmodel= new RequestGetlistModel();
		System.out.println(par.RequestToString(rqgetlistmodel));

		//GETMODEL
		LinkedList<String> parts = new LinkedList<String>();
		parts.add("atrs");
		parts.add("tpes");
		RequestGetModel rqgetmodel = new RequestGetModel("Piotrek", "termostat", "hmr", parts);
		System.out.println(par.RequestToString(rqgetmodel));

		//MODEL EXIST
		RequestExistModel rqesistmodel= new RequestExistModel("Piotrek", "termostat");
		System.out.println(par.RequestToString(rqesistmodel));

		//ADD MODEL
		RequestAddModel rqaddmodel = new RequestAddModel("Piotrek", "termostat", "xtype [ name: week_days, base: symbolic, ordered: yes, domain: [moday,tuesday], desc: \\'This is only one definition\\'].", "hmr");
		System.out.println(par.RequestToString(rqaddmodel));

		//REMOVE MODEL
		RequestRemoveModel rqremovemodel = new RequestRemoveModel("Piotrek", "termostat");
		System.out.println(par.RequestToString(rqremovemodel));

		//MODEL RUN
		RequestRunModel rqrunmodel = new RequestRunModel("Piotrek", "termostat", "[ms,dt,th,os]" , "[[day,monday], [hour,13], [month, january]]" , "ddi");
		System.out.println(par.RequestToString(rqrunmodel));

		//STATE ADD
		RequestAddState rqaddstate = new RequestAddState("Piotrek", "termostat", "s1", "[[day,monday], [hour,13], [month, january]]");
		System.out.println(par.RequestToString(rqaddstate));

		//STATE REMOVE
		RequestRemoveState rqremovestate = new RequestRemoveState("Piotrek", "termostat", "s1");
		System.out.println(par.RequestToString(rqremovestate));

		//MODEL VERIFY
		RequestVerifyModel rqverifymodel = new RequestVerifyModel("Piotrek", "termostat", "vreduce", "os");
		System.out.println(par.RequestToString(rqverifymodel));

		//CONSOLE QUESTION
		RequestConsole rqconsole = new RequestConsole("[model, exists, 'console', 'test'].");
		System.out.println(par.RequestToString(rqconsole));

		//TESTOWANIE REQUEST OFF -------------
	}
}
