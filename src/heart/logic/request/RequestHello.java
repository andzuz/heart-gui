package heart.logic.request;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 16:09
 */

public class RequestHello extends Request {
	String username;

	public RequestHello(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
}

