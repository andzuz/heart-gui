package heart.logic.request;

import java.util.List;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 16:09
 */

public class RequestGetModel extends Request {
	String username;
	String modelname;
	String format;
	List<String> parts;

	public RequestGetModel(String username, String modelname, String format, List<String> parts) {
		this.username = username;
		this.modelname = modelname;
		this.format = format;
		this.parts = parts;
	}

	public String getUsername() {
		return username;
	}

	public String getModelname() {
		return modelname;
	}

	public String getFormat() {
		return format;
	}

	public List<String> getParts() {
		return parts;
	}
}

