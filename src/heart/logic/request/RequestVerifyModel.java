package heart.logic.request;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 16:09
 */

public class RequestVerifyModel extends Request {
	String username;
	String modelname;
	String mode;
	String schema;

	public RequestVerifyModel(String username, String modelname, String mode, String schema) {
		this.username = username;
		this.modelname = modelname;
		this.mode = mode;
		this.schema = schema;
	}

	public String getUsername() {
		return username;
	}

	public String getModelname() {
		return modelname;
	}

	public String getMode() {
		return mode;
	}

	public String getSchema() {
		return schema;
	}
}

