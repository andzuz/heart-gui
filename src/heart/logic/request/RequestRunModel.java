package heart.logic.request;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 16:09
 */

public class RequestRunModel extends Request {
	String username;
	String modelname;
	String tables;
	String statenameORstatedef;
	String mode;

	public RequestRunModel(String username, String modelname, String tables, String statenameORstatedef, String mode) {
		this.username = username;
		this.modelname = modelname;
		this.tables = tables;
		this.statenameORstatedef = statenameORstatedef;
		this.mode = mode;
	}

	public String getMode() {
		return mode;
	}

	public String getUsername() {
		return username;

	}

	public String getModelname() {
		return modelname;
	}

	public String getTables() {
		return tables;
	}

	public String getStatenameORstatedef() {
		return statenameORstatedef;
	}
}

