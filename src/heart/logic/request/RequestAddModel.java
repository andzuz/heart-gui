package heart.logic.request;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 16:09
 */

public class RequestAddModel extends Request {
	String username;
	String modelname;
	String model;
	String format;

	public RequestAddModel(String username, String modelname, String model, String format) {
		this.username = username;
		this.modelname = modelname;
		this.model = model;
		this.format = format;
	}

	public String getUsername() {
		return username;
	}

	public String getModelname() {
		return modelname;
	}

	public String getModel() {
		return model;
	}

	public String getFormat() {
		return format;
	}
}

