package heart.logic.request;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 16:09
 */

public class RequestRemoveModel extends Request {
	String username;
	String modelname;

	public RequestRemoveModel(String username, String modelname) {
		this.username = username;
		this.modelname = modelname;
	}

	public String getUsername() {
		return username;
	}

	public String getModelname() {
		return modelname;
	}
}

