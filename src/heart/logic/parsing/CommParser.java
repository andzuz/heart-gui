package heart.logic.parsing;

import heart.logic.response.*;
import heart.logic.request.*;

//ZMIANA NAZWY W STOSUNKU DO DIAGRAMU - tamta nie miała sensu
public interface CommParser {
    Response stringToResponse(String raw, RespType type);
    String RequestToString(Request req);
}
