package heart.net;

import heart.logic.parsing.CommParser;
import heart.logic.parsing.Parser;
import heart.logic.request.*;
import heart.logic.response.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created with IntelliJ IDEA.
 * User: Andrzej
 * Date: 01.02.13
 * Time: 10:48
 * To change this template use File | Settings | File Templates.
 */
public class ConnectionHandler {
    private Socket sock;
    private PrintWriter out;
    private BufferedReader in;
    private CommParser parser;
    private String host;
    private int port;

    public ConnectionHandler(String host, int port, CommParser parser) {
        this.parser = parser;
        //localhost narazie
        this.host = host;
        this.port = port;
    }

    public void connect() throws IOException {
        sock = new Socket(host, port);
        out = new PrintWriter(sock.getOutputStream());
        in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
    }

    public Response sendAndGet(Request req, RespType type) throws IOException {
        connect();
        String strReq = parser.RequestToString(req);
        System.out.println("MAM REQ " + strReq);
        out.write(strReq + '\n');
        out.flush();

        String answer = "", line = "";
        while((line = in.readLine()) != null)
            answer += line;

        disconnect();
        return parser.stringToResponse(answer, type);
    }

    //wywoływać tę metodę w JFrame onClose
    public void disconnect() throws IOException {
        out.close();
        in.close();
        sock.close();
    }

}
