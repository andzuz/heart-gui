package heart.gui;

import heart.gui.heartComponents.*;
import heart.logic.request.RequestRemoveModel;
import heart.logic.response.*;
import heart.logic.request.*;
import heart.logic.response.RespType;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 16:19
 */

// Główna ramka - nasze główne zadanie
// Oparte na GridBagLayout, łatwiej zarządzać wielkością

public class FrameContent extends MainWindow {
	ArrayList<JButton> buttonList = new ArrayList<JButton>();
	ArrayList<ContentOfFrameContent> contentList = new ArrayList<ContentOfFrameContent>();

	ContentOfFrameContent temporaryContent;
	int nrActiveTab = 0;

	// Ilość przycisków wyznaczana na podstawie tej listy - easy to update później
	String[] buttonNames = {
			"MODEL ADD",
			"MODEL REMOVE",
			"MODEL GET",
			"MODEL RUN",
			"MODEL VERIFY"
	};

	public FrameContent() {
		contentList.add(new ModelAddContent());
		contentList.add(new ModelRemoveContent());
		contentList.add(new ModelGetContent());
		contentList.add(new ModelRunContent());
		contentList.add(new ModelVerifyContent());

		setGBCparameter(new GBC(0, 2).setWeight(100, 100).setFill(GBC.BOTH).setIpad(500, 400));
		setLayout(new GridBagLayout());
		setBorder(new EmptyBorder(0, 15, 3, 3));

		add(new ContentTabButtons(), new GBC(0, 0).setWeight(100,0).setFill(GBC.BOTH).setIpad(500,2));
		add(new SimpleLineOfFrameContent(), new GBC(0, 1).setWeight(100,0).setFill(GBC.BOTH));

		if(debugMode)
			setBackground(Color.GRAY);
	}

	class ContentTabButtons extends MainWindow {
		ContentTabButtons() {
			setLayout(new GridLayout(1, buttonNames.length, 15, 0));
			setBorder(new EmptyBorder(0, -2, 0, -2));

			for(int i = 0; i<buttonNames.length; i++ ) {
				HeartJButton button = new HeartJButton(buttonNames[i], HeartJButton.CONTENT_MENU);

				FrameContent.this.add(contentList.get(i), new GBC(0, 2).setWeight(100,100).setFill(GBC.BOTH));
				contentList.get(i).setVisible(false);
				temporaryContent = contentList.get(nrActiveTab);

				// ustawienie tylko pierwszego (bo domyślnie nrActiveTab==0) aktywnego
				if(i== nrActiveTab) {
					button.setForeground(Color.WHITE);
					button.setBackground(new Color(0, 122, 205));

					contentList.get(i).setVisible(true);
				}

				// Listener zmieniający kolor przycisku i dodatkowo ustawiający aktualny nrActiveTab
				final int finalI = i;
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						nrActiveTab = finalI;

						for (int i = 0; i < buttonNames.length; i++)
							// ustawienie aktywnego buttona w zależności od klikniecia
							if (i == nrActiveTab) {
								buttonList.get(i).setForeground(Color.WHITE);
								buttonList.get(i).setBackground(new Color(0, 122, 205));

								temporaryContent.setVisible(false);
								temporaryContent = contentList.get(nrActiveTab);
								temporaryContent.setVisible(true);
							} else {
								buttonList.get(i).setForeground(new Color(138, 138, 140));
								buttonList.get(i).setBackground(new Color(239, 238, 243));
							}
					}
				});

				// dodanie buttona do listy i do panelu
				buttonList.add(button);
				add(button);
			}
		}
	}

	// Niebieski paseczek - nie zmieniać, ładny :)
	class SimpleLineOfFrameContent extends MainWindow {
		SimpleLineOfFrameContent() {
			setMaxY(2);
			setFixedSize(getMaxX(), getMaxY());

			setBackground(new Color(0, 122, 205));
			add(new JLabel());
		}
	}
}

// Nasza główna klasa, trzebaby ją zrobić abstrakcyjną, bo w niej będą się zmieniać zawartości w zalezności od klikniętego przycisku,
// tutaj jest to puste pole pod buttonami
abstract class ContentOfFrameContent extends MainWindow {
	ContentOfFrameContent() {
		if(debugMode)
			setBackground(Color.PINK);
	}
}

class ModelAddContent extends ContentOfFrameContent {
	final HeartJLabel newModel = new HeartJLabel("New model:");
	final HeartJLabel currentModel = new HeartJLabel("Current model:");
	final HeartJLabel userName1 = new HeartJLabel("User name:");
	final HeartJLabel userName2 = new HeartJLabel("User name:");
	final HeartJLabel modelName1 = new HeartJLabel("Model name:");
	final HeartJLabel modelName2 = new HeartJLabel("Model name:");
	final HeartJLabel content1 = new HeartJLabel("Content:");
	final HeartJLabel content2 = new HeartJLabel("Content:");
	final HeartJLabel format1 = new HeartJLabel("Format:");
	final HeartJLabel format2 = new HeartJLabel("Format:");
	final HeartJButton copyInfo = new HeartJButton("COPY INFORMATION TO NEW MODEL", HeartJButton.NORMAL);
	final HeartJButton addModel = new HeartJButton("ADD MODEL                    ", HeartJButton.NORMAL);
	final HeartJRadioButton hmr1 = new HeartJRadioButton("hmr");
	final HeartJRadioButton hmr2 = new HeartJRadioButton("hmr");
	final HeartJRadioButton hml1 = new HeartJRadioButton("hml");
	final HeartJRadioButton hml2 = new HeartJRadioButton("hml");

	public static final HeartJTextField userName1Text = new HeartJTextField("");
	public static final HeartJTextField userName2Text = new HeartJTextField("");
	public static final HeartJTextField modelName1Text = new HeartJTextField("");
	public static final HeartJTextField modelName2Text = new HeartJTextField("");
	public static final HeartJTextPane content1Text = new HeartJTextPane().getEditable();
	public static final HeartJTextPane content2Text = new HeartJTextPane().getEditable();

	ModelAddContent() {
		setLayout(new GridBagLayout());
		copyInfo.setHorizontalAlignment(SwingConstants.CENTER);
		addModel.setHorizontalAlignment(SwingConstants.CENTER);

	    add(newModel, new GBC(0, 0).setFill(GBC.BOTH).setWeight(0, 0).setAllInsets(5));
		add(currentModel, new GBC(3,0).setFill(GBC.BOTH).setWeight(0,0).setAllInsets(5));
		add(userName1, new GBC(0,1).setFill(GBC.BOTH).setWeight(0,0).setInset(1,10,0,5));
		add(userName2, new GBC(3,1).setFill(GBC.BOTH).setWeight(0,0).setInset(1,10,0,5));
		add(modelName1, new GBC(0,2).setFill(GBC.BOTH).setWeight(0,0).setInset(1,10,0,5));
		add(modelName2, new GBC(3,2).setFill(GBC.BOTH).setWeight(0,0).setInset(1,10,0,5));
		add(format1, new GBC(0,3).setFill(GBC.BOTH).setWeight(0,0).setInset(1,10,0,5));
		add(format2, new GBC(3,3).setFill(GBC.BOTH).setWeight(0,0).setInset(1,10,0,5));
		add(content1, new GBC(0,4).setFill(GBC.BOTH).setWeight(0,100).setInset(1,10,0,5));
		add(content2, new GBC(3,4).setFill(GBC.BOTH).setWeight(0,100).setInset(1,10,0,5));

		add(userName1Text, new GBC(1,1,2,1).setFill(GBC.BOTH).setWeight(100,0).setInset(1,0,0,5));
		add(userName2Text, new GBC(4,1,2,1).setFill(GBC.BOTH).setWeight(100,0).setInset(1,0,0,5));
		add(modelName1Text, new GBC(1,2,2,1).setFill(GBC.BOTH).setWeight(100,0).setInset(1,0,0,5));
		add(modelName2Text, new GBC(4,2,2,1).setFill(GBC.BOTH).setWeight(100,0).setInset(1,0,0,5));
		add(content1Text.getScrollPane(), new GBC(1,4,2,1).setFill(GBC.BOTH).setWeight(100,100).setInset(1,0,0,5));
		add(content2Text.getScrollPane(), new GBC(4,4,2,1).setFill(GBC.BOTH).setWeight(100,100).setInset(1,0,0,5));
		add(hmr1, new GBC(1,3).setFill(GBC.BOTH).setWeight(0,0).setInset(1,0,0,5));
		add(hml1, new GBC(2,3).setFill(GBC.BOTH).setWeight(0,0).setInset(1,0,0,5));
		add(hmr2, new GBC(4,3).setFill(GBC.BOTH).setWeight(0,0).setInset(1,0,0,5));
		add(hml2, new GBC(5,3).setFill(GBC.BOTH).setWeight(0,0).setInset(1,0,0,5));

		add(copyInfo, new GBC(3,5,3,1).setFill(GBC.BOTH).setWeight(0,0).setInset(7,0,0,5));
		add(addModel, new GBC(0,5,3,1).setFill(GBC.BOTH).setWeight(0,0).setInset(7,0,0,5));

		addLogic();
	}

	// LOGIC SECTION


	void addLogic() {
		copyInfo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modelName1Text.setText(modelName2Text.getText());
				content1Text.setText(content2Text.getText());
			}
		});

		addModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                if((!hml1.isSelected() && !hmr2.isSelected()) ||
                        userName1Text.getText().length() == 0 ||
                        content1Text.getText().length() == 0) {
                    JOptionPane.showMessageDialog(null, "All fields must be filled correctly.");
                    return;
                }

                String format;
                if(hml1.isSelected())
                    format = "hml";
                else
                    format = "hmr";

               Response resp = null;

               try {
                   Request req =  new RequestAddModel(
                            userName1Text.getText(),
                            modelName1Text.getText(),
                            content1Text.getText(),
                            format
                   );

                   resp =
                        MainWindow.connectionHandler.sendAndGet(
                            req,
                            RespType.MODEL_ADD
                        );

                   System.out.println("Otrzymalem response: " + resp);
                   FrameConsole.updateJTestPane(req, resp);

               } catch(Exception ignoreForNow) {}

                userName1Text.setText("");
                modelName1Text.setText("");
                content1Text.setText("");

                //jeśli błąd
                if(resp instanceof ResponseError)
                    JOptionPane.showConfirmDialog(
                            null,
                            ((ResponseError) resp).getRaw()
                    );
			}
		});
	}
}

class ModelRemoveContent extends ContentOfFrameContent {
	final HeartJButton removeModel = new HeartJButton("REMOVE CURRENT MODEL", HeartJButton.NORMAL);

	ModelRemoveContent() {
		setLayout(new GridBagLayout());
		removeModel.setHorizontalAlignment(SwingConstants.CENTER);

		add(removeModel, new GBC(0, 0).setFill(GBC.BOTH).setWeight(0, 0).setAllInsets(50));

		addLogic();
	}

	// LOGIC SECTION

	void addLogic() {
		removeModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                String modelname = ModelAddContent.modelName2Text.getText();
                String username = ModelAddContent.userName2Text.getText();

                try {
                    Request req = new RequestRemoveModel(username,modelname);

                    Response resp = MainWindow.connectionHandler.sendAndGet(
                        req,
                        RespType.MODEL_REMOVE
                    );

                    if(resp instanceof ResponseError)
                        JOptionPane.showMessageDialog(null,
                                ((ResponseError)resp).getRaw());

                    FrameConsole.updateJTestPane(req, resp);

                } catch(Exception ignoreForNow) {}
			}
		});

	}
}

class ModelGetContent extends ContentOfFrameContent {
	final HeartJLabel format = new HeartJLabel("Format:");
	final HeartJLabel parts = new HeartJLabel("Parts:");
	final HeartJLabel content = new HeartJLabel("Content:");

	final HeartJRadioButton hmr = new HeartJRadioButton("hmr");
	final HeartJRadioButton hml = new HeartJRadioButton("hml");

	final HeartJButton getCurrentModel = new HeartJButton("GET CURRENT MODEL", HeartJButton.NORMAL);

	final HeartJTextPane contentOfModel = new HeartJTextPane();
	final HeartJTextField partsField = new HeartJTextField("");


	ModelGetContent() {
		setLayout(new GridBagLayout());
		getCurrentModel.setHorizontalAlignment(SwingConstants.CENTER);
		content.setHorizontalAlignment(SwingConstants.CENTER);

		add(format, new GBC(0, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(5, 0, 3, 10));
		add(hmr, new GBC(1, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 15, 0, 15));
		add(hml, new GBC(2, 0).setFill(GBC.BOTH).setWeight(0, 0));
		add(parts, new GBC(0, 1).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 10));
		add(partsField, new GBC(1, 1, 2 ,1).setFill(GBC.BOTH).setWeight(100, 0));
		add(getCurrentModel, new GBC(0, 2, 3, 1).setFill(GBC.BOTH).setWeight(0, 0).setAllInsets(10));
		add(content, new GBC(0, 3).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 0));
		add(contentOfModel.getScrollPane(), new GBC(0, 4, 3, 1).setFill(GBC.BOTH).setWeight(100, 100));

		addLogic();
	}

	// LOGIC SECTION

	void addLogic() {
		getCurrentModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO: Pobranie aktualnego modelu
                try {
                    //TODO: Poprawić niezgodność typów!!
                   /* MainWindow.connectionHandler.sendAndGet(
                        new RequestGetModel(
                                ModelAddContent.userName2Text.getText(),
                                ModelAddContent.modelName2Text.getText(),
                                format.getText(),
                                parts.getText()
                        ),
                        RespType.MODEL_GET
                    );                 */

                } catch(Exception ignoreForNow) {}
			}
		});
	}
}

class ModelRunContent extends ContentOfFrameContent {
	final HeartJLabel mode = new HeartJLabel("Mode:");
	final HeartJLabel tables = new HeartJLabel("Tables:");
	final HeartJLabel statenameOrStatedef = new HeartJLabel("Statename or Statedef:");
	final HeartJLabel output = new HeartJLabel("Output:");

	final HeartJRadioButton ddi = new HeartJRadioButton("ddi");
	final HeartJRadioButton gdi = new HeartJRadioButton("gdi");
	final HeartJRadioButton tdi = new HeartJRadioButton("tdi");
	final HeartJRadioButton foi = new HeartJRadioButton("foi");

	final HeartJButton runCurrentModel = new HeartJButton("RUN CURRENT MODEL", HeartJButton.NORMAL);

	final HeartJTextPane outputText = new HeartJTextPane();
	final HeartJTextField tablesText = new HeartJTextField("");
	final HeartJTextField stnOrStd = new HeartJTextField("");

	ModelRunContent() {
		setLayout(new GridBagLayout());
		runCurrentModel.setHorizontalAlignment(SwingConstants.CENTER);

		add(mode, new GBC(0, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 0));
		add(tables, new GBC(0, 1).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 0));
		add(statenameOrStatedef, new GBC(0, 2).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 10));
		add(ddi, new GBC(1, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 15, 0, 15));
		add(gdi, new GBC(2, 0).setFill(GBC.BOTH).setWeight(0, 0));
		add(tdi, new GBC(3, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 15, 0, 15));
		add(foi, new GBC(4, 0).setFill(GBC.BOTH).setWeight(0, 0));
		add(tablesText, new GBC(1, 1, 4, 1).setFill(GBC.BOTH).setWeight(100, 0).setInset(0, 0, 3, 0));
		add(stnOrStd, new GBC(1, 2, 4, 1).setFill(GBC.BOTH).setWeight(100, 0).setInset(0, 0, 3, 0));
		add(runCurrentModel, new GBC(0, 3, 5, 1).setFill(GBC.BOTH).setWeight(100, 0).setInset(6, 0, 6, 0));
		add(output, new GBC(0, 4).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 0));
		add(outputText.getScrollPane(), new GBC(0, 5, 5, 1).setFill(GBC.BOTH).setWeight(100, 100).setInset(0, 0, 3, 0));


		addLogic();
	}

	// LOGIC SECTION

	void addLogic() {
		runCurrentModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

                if((!ddi.isSelected() && !tdi.isSelected() &&
                !foi.isSelected() && !gdi.isSelected()) ||
                        tables.getText().length() == 0 ||
                        stnOrStd.getText().length() == 0) {
                    JOptionPane.showMessageDialog(null, "All fields must be filled correctly.");
                    return;
                }

				//TODO: Uruchomienie modelu
                try {
                  String mode = "";
                  if(ddi.isSelected())
                      mode = "ddi";
                  else if(gdi.isSelected())
                      mode = "gdi";
                  else if(tdi.isSelected())
                      mode = "tdi";
                  else if(foi.isSelected())
                      mode = "foi";

                  Request req = new RequestRunModel(ModelAddContent.userName2Text.getText(),
                          ModelAddContent.modelName2Text.getText(),
                          tablesText.getText(),
                          stnOrStd.getText(),
                          mode
                  );

                  Response resp = MainWindow.connectionHandler.sendAndGet(
                    req,
                    RespType.MODEL_RUN
                  );

                  tablesText.setText("");
                  stnOrStd.setText("");
                  foi.setSelected(false);
                  gdi.setSelected(false);
                  tdi.setSelected(false);
                  ddi.setSelected(false);

                  if(resp instanceof ResponseError)
                      JOptionPane.showMessageDialog(null,
                              ((ResponseError)resp).getRaw());
                  else
                      outputText.setText(
                              ((ResponseRunModel)resp).getRaw()
                      );

                  FrameConsole.updateJTestPane(req, resp);

                } catch(Exception ignoreForNow) {

                }
			}
		});
	}
}

class ModelVerifyContent extends ContentOfFrameContent {
	final HeartJLabel mode = new HeartJLabel("Mode:");
	final HeartJLabel schema = new HeartJLabel("Schema:");
	final HeartJLabel output = new HeartJLabel("Output:");

	final HeartJRadioButton vcomplete = new HeartJRadioButton("vcomplete");
	final HeartJRadioButton vcontradict = new HeartJRadioButton("vcontradict");
	final HeartJRadioButton vsubsume = new HeartJRadioButton("vsubsume");
	final HeartJRadioButton vreduce = new HeartJRadioButton("vreduce");

	final HeartJButton verifyCurrentModel = new HeartJButton("VERIFY CURRENT MODEL", HeartJButton.NORMAL);

	final HeartJTextPane outputText = new HeartJTextPane();
	final HeartJTextField schemaText = new HeartJTextField("");

	ModelVerifyContent() {
		setLayout(new GridBagLayout());
		verifyCurrentModel.setHorizontalAlignment(SwingConstants.CENTER);

		add(mode, new GBC(0, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 0));
		add(schema, new GBC(0, 1).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 15));
		add(vcomplete, new GBC(1, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 15, 0, 15));
		add(vcontradict, new GBC(2, 0).setFill(GBC.BOTH).setWeight(0, 0));
		add(vsubsume, new GBC(3, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 15, 0, 15));
		add(vreduce, new GBC(4, 0).setFill(GBC.BOTH).setWeight(0, 0));
		add(schemaText, new GBC(1, 1, 4, 1).setFill(GBC.BOTH).setWeight(100, 0).setInset(0, 0, 3, 0));
		add(verifyCurrentModel, new GBC(0, 2, 5, 1).setFill(GBC.BOTH).setWeight(100, 0).setInset(6, 0, 6, 0));
		add(output, new GBC(0, 3).setFill(GBC.BOTH).setWeight(0, 0).setInset(0, 0, 3, 0));
		add(outputText.getScrollPane(), new GBC(0, 4, 5, 1).setFill(GBC.BOTH).setWeight(100, 100).setInset(0, 0, 3, 0));

		addLogic();
	}

	// LOGIC SECTION

	void addLogic() {
		verifyCurrentModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

                if((!vcomplete.isSelected() && !vcontradict.isSelected()
                        && !vsubsume.isSelected() && !vreduce.isSelected()) ||
                        schemaText.getText().length() == 0 ||
                        ModelAddContent.modelName2Text.getText().length() == 0) {
                    JOptionPane.showMessageDialog(null, "All fields must be filled correctly.\n" +
                                                "(or current model is empty)");
                    return;
                }

                String mode = "";
                if(vcomplete.isSelected())
                    mode = "vcomplete";
                else if(vcontradict.isSelected())
                    mode = "vcontradict";
                else if(vsubsume.isSelected())
                    mode = "vsubsume";
                else if(vreduce.isSelected())
                    mode = "vreduce";

                try {

                    Request req = new RequestVerifyModel(
                            ModelAddContent.userName2Text.getText(),
                            ModelAddContent.modelName2Text.getText(),
                            mode,
                            schemaText.getText()
                    );

                    Response resp = MainWindow.connectionHandler.sendAndGet(
                        req,
                        RespType.MODEL_VERIFY
                    );

                    FrameConsole.updateJTestPane(req, resp);

                    if(resp instanceof ResponseError)
                        JOptionPane.showMessageDialog(null, ((ResponseError)resp).getRaw());
                    else {
                        String[] resArr =  ((ResponseVerifyModel)resp).asArray();
                        String resStr = "";

                        for(String s : resArr) {
                            resStr = resArr + ", ";
                        }

                        outputText.setText(resStr.substring(0, resStr.length() - 1));
                    }
                } catch(Exception ignoreForNow) {}
			}
		});
	}
}
