package heart.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 15:54
 */

/* Klasa FrontFrame zawiera zbiór obiektów dziedziczących po głównej klasie okna MainWindow, która dziedziczy po JPanel
 * Wszystko opiera się na GridBagLayout wykorzustując klasę GBC do łatwiejszego zarządzania rozkładem.
 */

public class FrontFrame extends JFrame {
	// Nagłówek - w domyśle będzie emulował wygląd paska w systemach innych niż win
	// potrzebuje parametru this do zarządzania minimalizowaniem i zamykaniem okna
	FrameHeader header = new FrameHeader(this);
	// Nagłówek menu - zawiera m.in przycisk Switch User
	FrameMenu menu = new FrameMenu();
	// Główna ramka zawartości - zawiera całą zawartość dotyczącą obsługi konkretnego polecenia, np. Model Add, Verify Model
	FrameContent content = new FrameContent();
	// Ozdobnik - przerwa między ramką zawartości a model explorerem
	FrameBreakPanel panelBreak = new FrameBreakPanel();
	// Model Explorer - zawiera elementy operacji na modelach i stanach
	FrameModelExplorer modelExplorer = new FrameModelExplorer();
	// Okno konsoli
	FrameConsole console = new FrameConsole();
	// Stopka
	FrameFotter fotter = new FrameFotter();

	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	final int WIDTH = 1200;
	final int HEIGHT = 600;

	public FrontFrame() {

		// Wyomantowane fragmanty pozwalają ukryć obramowanie, należałoby to zrobić i sprawdzić jak wygląda na różnych systemach
		// po stworzeniu odpowiednich przycisków i emulacji przesuwania okna
		setUndecorated(true);
		getRootPane().setWindowDecorationStyle(JRootPane.NONE);

		setLayout(new GridBagLayout());
		setLocation((int) (screenSize.getWidth()-WIDTH)/2, (int) (screenSize.getHeight()-HEIGHT)/2);
		setSize(WIDTH,HEIGHT);

		add(header, header.getGBCparameter());
		add(menu, menu.getGBCparameter());
		add(content, content.getGBCparameter());
		add(panelBreak, panelBreak.getGBCparameter());
		add(modelExplorer, modelExplorer.getGBCparameter());
		add(console, console.getGBCparameter());
		add(fotter, fotter.getGBCparameter());

		final Boolean[] right = new Boolean[1];
		final Boolean[] down = new Boolean[1];

		addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				int width = 500;
				int height = 400;

				if(right[0] || down[0]) {
					width = e.getLocationOnScreen().x - getLocation().x;
					height = e.getLocationOnScreen().y - getLocation().y;

					if(width<500)
						width = 500;
					if(height<400)
						height = 400;
				}

				if (right[0] && down[0])
					setSize(width, height);
				else if (right[0])
					setSize(width,getHeight());
				else if (down[0])
					setSize(getWidth(), height);
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				right[0] = getLocation().x + getWidth() - 7 < e.getLocationOnScreen().x;
				down[0] = getLocation().y + getHeight() - 7 < e.getLocationOnScreen().y;

				if (right[0] && down[0])
					setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
				else if (right[0])
					setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
				else if (down[0])
					setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
				else
					setCursor(Cursor.getDefaultCursor());
			}
		});
	}
}