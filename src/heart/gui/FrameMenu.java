package heart.gui;

import heart.gui.heartComponents.*;
import heart.logic.request.*;
import heart.logic.response.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 16:17
 */

	// Proste menu z przyciskami, raczej element rozwojowy, nie wiadomo co nam tu wskoczy.

public class FrameMenu extends MainWindow {
	final HeartJButton switchUser = new HeartJButton("SWITCH USER    ", HeartJButton.NORMAL);
	final HeartJButton createAccount = new HeartJButton("CREATE ACCOUNT", HeartJButton.NORMAL);
	final HeartJButton connectToServer = new HeartJButton("CONNECT TO SERVER", HeartJButton.NORMAL);



	public FrameMenu() {
		setMaxY(25);
		setFixedSize(getMaxX(),getMaxY());
		setGBCparameter(new GBC(0,1,3,1).setWeight(100,0).setFill(GBC.BOTH).setIpad(getMinX(),getMaxY()));
		setBorder(new EmptyBorder(5, 10, 5, 15));

		setLayout(new BorderLayout());

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonsPanel.add(switchUser);
		//buttonsPanel.add(createAccount);  Przyjdzie czas na ten przycisk :)

		add(buttonsPanel, BorderLayout.WEST);
		add(connectToServer, BorderLayout.EAST);

		addLogic();

		if(debugMode)
			setBackground(Color.BLUE);
	}

	// LOGIC SECTION

	void addLogic() {
		final HeartJButton chooseUserButton = new HeartJButton("CHOOSE", HeartJButton.NORMAL);
		final HeartJButton cancel = new HeartJButton("CANCEL", HeartJButton.NORMAL);

		switchUser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final HeartFramePopUp popUp = new HeartFramePopUp(new Dimension(300,170), " CHOOSE");
				final JPanel panelInsidePopUp = new JPanel();

				panelInsidePopUp.setLayout(new GridBagLayout());
				panelInsidePopUp.setBackground(new Color(239, 238, 243));

				HeartJLabel chooseUser = new HeartJLabel("Choose user:");
				final JComboBox<String> listUsers;

				if(getAllUsers()!=null) {
					listUsers = new JComboBox<String>(getAllUsers());

					chooseUserButton.setHorizontalAlignment(SwingConstants.CENTER);
					chooseUserButton.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							currentUser = (String) listUsers.getSelectedItem();
							currentUserNr = listUsers.getSelectedIndex();
							currentModelNr = -1;

							try {
								updateCurrentUser();
								updateCurrentModel();
							} catch (IOException e1) {
								e1.printStackTrace();
							}

							chooseUserButton.removeActionListener(this);

							popUp.setVisible(false);
							popUp.dispose();
						}
					});

					panelInsidePopUp.add(chooseUser, new GBC(0, 0, 2, 1).setWeight(100, 100).setFill(GBC.BOTH).setAllInsets(10));
					panelInsidePopUp.add(listUsers, new GBC(0, 1, 2, 1).setWeight(100, 100).setFill(GBC.BOTH).setAllInsets(10));
					panelInsidePopUp.add(chooseUserButton, new GBC(0, 2).setWeight(100, 100).setFill(GBC.BOTH).setAllInsets(10));
					panelInsidePopUp.add(cancel, new GBC(1, 2).setWeight(100, 100).setFill(GBC.BOTH).setAllInsets(10));
				}
				else {
					popUp.setSize(new Dimension(200,100));
					HeartJLabel l = new HeartJLabel("Error. Connect first.");
					l.setForeground(Color.RED);
					panelInsidePopUp.add(l, new GBC(0, 0).setWeight(100, 100).setFill(GBC.BOTH));
					panelInsidePopUp.add(cancel, new GBC(0, 1).setWeight(100, 100).setFill(GBC.BOTH));
				}

				cancel.setHorizontalAlignment(SwingConstants.CENTER);
				cancel.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent event) {
						popUp.setVisible(false);
						popUp.dispose();
					}
				});

				popUp.addPanel(panelInsidePopUp);

				popUp.setVisible(true);
			}
		});

		final HeartJTextField hostText = new HeartJTextField("localhost");
		final HeartJTextField portText = new HeartJTextField("8090");
		final HeartJButton connect = new HeartJButton("CONNECT", HeartJButton.NORMAL);

		connectToServer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				final HeartFramePopUp popUp = new HeartFramePopUp(new Dimension(300,170), " CONNECT");
				final JPanel panelInsidePopUp = new JPanel();

				panelInsidePopUp.setLayout(new GridBagLayout());
				panelInsidePopUp.setBackground(new Color(239, 238, 243));

				HeartJLabel title = new HeartJLabel("Connect to:");
				HeartJLabel host = new HeartJLabel("Host:");
				HeartJLabel port = new HeartJLabel("Port:");

				connect.setHorizontalAlignment(SwingConstants.CENTER);
				connect.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent event) {
						try {
							if(establishConnection(hostText.getText(), portText.getText())) {
								RequestGetlistModel getList = new RequestGetlistModel();
								ResponseGetlistModel respGetList = (ResponseGetlistModel) connectionHandler.sendAndGet(getList, RespType.MODEL_GETLIST);

								FrameConsole.updateJTestPane(getList, respGetList);

								usersAndModels = respGetList.asHashMap();
								currentUser = getUser(currentUserNr);
								currentModelNr = -1;

								updateCurrentUser();
								updateCurrentModel();

								popUp.setVisible(false);
								popUp.dispose();

								connectToServer.setText("CONNECTED TO " + hostText.getText() + ":" + portText.getText());
								connectToServer.setEnabled(false);
							}
						} catch (IOException e) {
							HeartJLabel l = new HeartJLabel("Error. Unable to connect. Retry.");
							l.setForeground(Color.RED);
							panelInsidePopUp.add(l, new GBC(0, 5, 2, 1).setWeight(0, 0).setFill(GBC.BOTH));
							popUp.setSize(new Dimension(300,200));
							popUp.repaint();
							popUp.revalidate();
						}
					}
				});

				cancel.setHorizontalAlignment(SwingConstants.CENTER);
				cancel.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent event) {
						popUp.setVisible(false);
						popUp.dispose();
					}
				});

				panelInsidePopUp.add(title, new GBC(0, 1, 2, 1).setWeight(0, 0).setFill(GBC.BOTH).setIpad(150, 20));
				panelInsidePopUp.add(host, new GBC(0, 2).setWeight(0, 0).setFill(GBC.BOTH).setIpad(40, 0));
				panelInsidePopUp.add(port, new GBC(0, 3).setWeight(0, 0).setFill(GBC.BOTH).setIpad(40, 0));
				panelInsidePopUp.add(hostText, new GBC(1, 2).setWeight(0, 0).setFill(GBC.BOTH));
				panelInsidePopUp.add(portText, new GBC(1, 3).setWeight(0, 0).setFill(GBC.BOTH));
				panelInsidePopUp.add(connect, new GBC(0, 4, 1, 1).setWeight(0, 0).setIpad(0, 40).setFill(GBC.BOTH));
				panelInsidePopUp.add(cancel, new GBC(1, 4, 1, 1).setWeight(0, 0).setIpad(0, 40).setFill(GBC.BOTH));

				popUp.addPanel(panelInsidePopUp);
			}
		});
	}
}
