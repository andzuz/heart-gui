package heart.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 16:20
 */

// Po prostu panel z przerwą - nic ciekawego, wszystko zrobione :E

public class FrameBreakPanel extends MainWindow {
	public FrameBreakPanel() {
		setMaximumSize(new Dimension(10, 2000));
		setGBCparameter(new GBC(1,2).setWeight(0, 100).setFill(GBC.BOTH));

		setLayout(new BorderLayout());

		JLabel l = new JLabel(new ImageIcon(getBreakOrnament()));

		add(l, BorderLayout.CENTER);
		repaint();

		if(debugMode)
			setBackground(Color.RED);
	}
}
