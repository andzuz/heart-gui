package heart.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 15:54
 */

public class MainGUI {
	public static void main(String[] s) {
		// JDK BUGfix
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

		EventQueue.invokeLater( new Runnable() {
			public void run() {
				FrontFrame f = new FrontFrame();
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setVisible(true);
			}
		});
	}
}

