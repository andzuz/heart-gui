package heart.gui.heartComponents;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Created by: Piotrek
 * Date: 10.02.13
 * Time: 13:24
 */

public class HeartJTextField extends JTextField {
	String defaultText;
	boolean focusChangeEnabled = true;

	public HeartJTextField(String text) {
		super(text);
		this.defaultText = text;

		setBackground(new Color(252, 252, 252));
		setBorder(null);
		setForeground(new Color(178, 178, 180));
		setFont(new Font("Microsoft Yi Baiti", Font.ITALIC, 18));

		addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				if(focusChangeEnabled)
					if (getText().equals(defaultText)){
						setText("");
						setFont(new Font("Microsoft Yi Baiti", Font.PLAIN, 18));
						setForeground(Color.BLACK);
					}
			}

			public void focusLost(FocusEvent e) {
				if(focusChangeEnabled)
					if (getText().trim().equals("")){
						setText(defaultText);
						setFont(new Font("Microsoft Yi Baiti", Font.ITALIC, 18));
						setForeground(new Color(178, 178, 180));
					}
			}
		});
	}

	public HeartJTextField focusChangeDisabled() {
		focusChangeEnabled = false;
		setFont(new Font("Microsoft Yi Baiti", Font.PLAIN, 18));
		setForeground(Color.BLACK);

		return this;
	}
}
