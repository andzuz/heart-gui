package heart.gui.heartComponents;

import heart.gui.GBC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

/**
 * Created by: Piotrek
 * Date: 10.02.13
 * Time: 13:58
 */

public class HeartFramePopUp extends JFrame {
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	JPanel tempPanel = new JPanel();
	Dimension windowSize;
	final Point offset = new Point();

	public HeartFramePopUp(Dimension windowSize, String message) throws HeadlessException {
		this.windowSize = windowSize;
		setSize(windowSize.width, windowSize.height);
		setLayout(new GridBagLayout());
		setUndecorated(true);
		getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		setLocation((screenSize.width - 300) / 2, (screenSize.height - 170) / 2);

		tempPanel.setBackground(new Color(0, 122, 205));
		tempPanel.setLayout(new GridBagLayout());
		tempPanel.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				setLocation(e.getLocationOnScreen().x + offset.x, e.getLocationOnScreen().y + offset.y);
			}
		});
		tempPanel.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Point mouse = e.getLocationOnScreen();
				Point window = getLocation();

				offset.setLocation(window.x - mouse.x, window.y - mouse.y);
			}
		});

		HeartJLabel l = new HeartJLabel(message);
		l.setBackground(new Color(239, 238, 243));
		l.setOpaque(true);

		tempPanel.add(new HeartButtonsNavigate(this).setExitOnly(), new GBC(1, 0).setFill(GBC.BOTH).setWeight(0, 0).setInset(3, 0, 1, 3));
		tempPanel.add(l, new GBC(0, 0).setFill(GBC.BOTH).setWeight(100,0).setInset(3, 3, 1, 0));

		add(tempPanel, new GBC(0, 0).setFill(GBC.BOTH).setWeight(100, 100).setIpad(windowSize.width/2, windowSize.height/2));
	}

	public void addPanel(JPanel panel) {
		tempPanel.add(panel, new GBC(0, 1 ,2 ,1).setFill(GBC.BOTH).setInset(0, 3, 3, 3).setWeight(100,100));
		tempPanel.repaint();
		setVisible(true);
	}
}
