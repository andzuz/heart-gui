package heart.gui.heartComponents;

import heart.gui.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 10.02.13
 * Time: 13:11
 */

public class HeartHeader extends MainWindow {
	public HeartHeader(String text) {
		HeartJLabel labelText = new HeartJLabel(" " + text + " ");

		setLayout(new FlowLayout(FlowLayout.LEFT,0,-2));
		setBorder(new EmptyBorder(0,0,3,0));

		for(int i = 0; i < 300; i++)
			add(new JLabel(new ImageIcon(getOrnamentHorizontal())), FlowLayout.LEFT);

		add(labelText, FlowLayout.LEFT);
		add(new JLabel(new ImageIcon(getOrnamentVertical())), FlowLayout.LEFT);
	}
}
