package heart.gui.heartComponents;

import javax.swing.*;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 10.02.13
 * Time: 13:12
 */

public class HeartJLabel extends JLabel {
	public HeartJLabel(String text) {
		super(text);
		setFont(new Font("Microsoft Yi Baiti", Font.PLAIN, 18));
		//setForeground(new Color(138, 138, 140));
		setForeground(Color.BLACK);
	}
}
