package heart.gui.heartComponents;

import heart.gui.MainWindow;

import javax.swing.*;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 10.02.13
 * Time: 12:07
 */

public class HeartJButton extends JButton {
	public static final int NORMAL = 1;
	public static final int EXIT = 2;
	public static final int MINIMIZE = 3;
	public static final int MAXIMIZE = 4;
	public static final int SEARCH = 5;
	public static final int CONTENT_MENU = 6;


	public HeartJButton(int type) {
		super();

		setHorizontalAlignment(SwingConstants.LEFT);
		setFont(new Font("Microsoft Yi Baiti", Font.PLAIN, 18));
		//setForeground(new Color(138, 138, 140));
		setForeground(Color.BLACK);
		setBackground(new Color(239, 238, 243));
		setBorder(null);

		if(type == HeartJButton.EXIT) {
			setBackground(null);
			setIcon(MainWindow.getExitIcon());
			setRolloverIcon(MainWindow.getExitIconOn());
		}

		if(type == HeartJButton.MINIMIZE) {
			setBackground(null);
			setIcon(MainWindow.getMinimizeIcon());
			setRolloverIcon(MainWindow.getMinimizeIconOn());
			setDisabledIcon(MainWindow.getMinimizeIconOff());
		}

		if(type == HeartJButton.MAXIMIZE) {
			setBackground(null);
			setIcon(MainWindow.getMaximizeIcon());
			setRolloverIcon(MainWindow.getMaximizeIconOn());
			setDisabledIcon(MainWindow.getMaximizeIconOff());
		}

		if(type == HeartJButton.SEARCH) {
			setBackground(null);
			setIcon(MainWindow.getSearchIcon());
		}
	}

	public HeartJButton(String text, int type) {
		super(text);

		setHorizontalAlignment(SwingConstants.LEFT);
		setFont(new Font("Microsoft Yi Baiti", Font.PLAIN, 18));
		//setForeground(new Color(138, 138, 140));
		setForeground(Color.BLACK);
		setBackground(new Color(239, 238, 243));
		setBorder(null);

		if(type == HeartJButton.NORMAL) {
			setIcon(MainWindow.getButtonIcon());
		}
		if(type == HeartJButton.CONTENT_MENU) {
			setForeground(new Color(138, 138, 140));
			setHorizontalAlignment(SwingConstants.CENTER);
			setIcon(null);
		}
	}


}
