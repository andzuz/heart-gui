package heart.gui;

import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 15:53
 */

public class GBC extends GridBagConstraints {
	public GBC(int gridx, int gridy) {
		this.gridx = gridx;
		this.gridy = gridy;
	}

	public GBC(int gridx, int gridy, int gridwidth, int gridheight) {
		this.gridx = gridx;
		this.gridy = gridy;
		this.gridwidth = gridwidth;
		this.gridheight = gridheight;
	}

	//ustawia parametr anchor
	public GBC setAnchor(int anchor) {
		this.anchor = anchor;
		return this;
	}

	// Ustawia kierunek zapełniania
	public GBC setFill(int fill) {
		this.fill = fill;
		return this;
	}

	//Ustawia parametry weight komórek
	public GBC setWeight(double weightx, double weighty) {
		this.weightx = weightx;
		this.weighty = weighty;
		return this;
	}

	// ustawia pustą przestrzeń w komórce we wszystkich kierunkach
	public GBC setAllInsets(int distance) {
		this.insets = new Insets(distance, distance, distance, distance);
		return this;
	}

	// ustawia pustą przestrzeń w komórce w dowolnym kierunku
	public GBC setInset(int top, int left, int bottom, int right) {
		this.insets = new Insets(top, left, bottom, right);
		return this;
	}

	// ustawia wypełnienie wewnętrzne
	public GBC setIpad(int ipadx, int ipady) {
		this.ipadx = ipadx;
		this.ipady = ipady;
		return this;
	}
}
